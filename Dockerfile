FROM ubuntu:latest

RUN apt-get -y update; \
    apt-get -y upgrade; \
    apt-get -y install apt-utils; \
	apt-get -y install curl nano; \
    apt-get -y install logrotate; 

ENTRYPOINT logrotate -vf /etc/logrotate.conf > /logs/filebeat/logrotate